import * as express from "express";
import { Express, Request, Response } from "express";
import { FilterDTO } from "./dtos/filter";
import { BookDTO } from "./dtos/book";
import { BookService } from "./services/book";
import { connect } from 'mongoose';

run().catch(err => console.log(err));

async function run() {
  // 4. Connect to MongoDB
  await connect('mongodb://127.0.0.1:27017/test');
}

const bookService = new BookService();

const app = express()
app.use(express.json());
const port = 3000

app.get('/', (req: Request, res: Response) => {
    res.end('Welcome');
});

app.get('/books', async (req: Request, res: Response) => {
    const query = req.query;
    const filter: FilterDTO = {
        name: query?.name,
        author: query?.author,
        priceFrom: query?.priceFrom,
        priceTo: query?.priceTo
    };
    const list = await bookService.list(filter);
    res.json({
        status: 'ok',
        data: list
    });
})

app.get('/books/:id', async (req: Request, res: Response) => {
    const id = req.params.id;
    const detail = await bookService.findBookById(id);
    res.json({
        status: 'ok',
        data: detail
    });
})

app.post('/books', async (req: Request, res: Response) => {
    const body = req.body;
    const book: BookDTO = {
        name: body.name,
        author: body.author,
        price: body.price
    };
    try {
        const result = await bookService.insert(book);
        return res.json({
            status: 'ok',
            data: result
        });
    } catch (e) {
        console.log(e);
    }
    return res.status(500).json({
        status: 'fail'
    });
})

app.patch('/books/:id', async (req: Request, res: Response) => {
    const body = req.body;
    const book: BookDTO = {
        name: body.name,
        author: body.author,
        price: body.price
    };
    const id = req.params.id;
    try {
        const result = await bookService.update(id, book);
        return res.json({
            status: 'ok',
            data: result
        });
    } catch (e) {
        console.log(e);
    }
    return res.status(500).json({
        status: 'fail'
    });
})

app.delete('/books/:id', async (req: Request, res: Response) => {
    const id = req.params.id;console.log(id);
    try {
        await bookService.delete(id);
        return res.json({
            status: 'ok'
        });
    } catch (e) {
        console.log(e);
    }
    return res.status(500).json({
        status: 'fail'
    });
})

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})