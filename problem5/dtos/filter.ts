export interface FilterDTO
{
    name?: string
    author?: string
    priceFrom?: number
    priceTo?: number
}