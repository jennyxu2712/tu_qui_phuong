export interface BookDTO
{
    name: string
    price: number
    author: string
}