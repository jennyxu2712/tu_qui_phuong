import { ObjectId } from 'bson';
import { Schema, model, connect } from 'mongoose';

// 1. Create an interface representing a document in MongoDB.
interface IBook {
    _id: string,
    name: string;
    price: number;
    author: string;
}

const bookSchema = new Schema<IBook>({
    name: { type: String, required: true },
    price: { type: Number, required: true },
    author: { type: String, required: true }
}); 

export var Book = model<IBook>('Book', bookSchema);