import { Book } from "../models/book";
import { FilterDTO } from "../dtos/filter";
import { BookDTO } from "../dtos/book";

export class BookService
{

    list(filter: FilterDTO) {
        let query = Book.find();
        if (filter.name) {
            query.where('name').equals(filter.name);
        }
        if (filter.author) {
            query.where('author').equals(filter.author);
        }
        if (filter.priceFrom) {
            query.where('price').gte(filter.priceFrom);
        }
        if (filter.priceTo) {
            query.where('price').lte(filter.priceTo);
        }
        return query;
    }

    insert(book: BookDTO) {
        const bookModel = this._getModel(book);
        return bookModel.save();
    }

    async update(id: string, book: BookDTO) {
        const doc = await this.findBookById(id);
        if (doc) {
            return Book.findByIdAndUpdate(id, book);
        }
        throw new Error('Book not found');
    }

    async delete(id: string) {
        const doc = await this.findBookById(id);
        if (doc) {
            return Book.findByIdAndDelete(id);
        }
        throw new Error('Book not found');
    }

    findBookById(id: string) {
        return Book.findById(id);
    }

    _getModel(book?: BookDTO) {
        return new Book(book);
    }
}