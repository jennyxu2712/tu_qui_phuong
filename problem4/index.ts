function sum_to_n_a(n: number): number {
	let start: number = 1;
	let result: number = 0;
	while (start <= n) {
		result += start;
		start++;
	}
	return result;
}

function sum_to_n_b(n: number): number {
	return n * (n + 1) / 2;
}

function sum_to_n_c(n: number): number {
	const isOdd: boolean = n % 2 === 0;
	if (isOdd) {
		return (n / 2) * n + n / 2;
	}
	return Math.ceil(n / 2) * n;
}