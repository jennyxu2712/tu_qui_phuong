# Problem6
## Appendix
- Rquirements
- Implement
- Diagram
- Optimizations
## Requirements
    1. We have a website with a score board, which shows the top 10 user’s scores.
    2. We want live update of the score board.
    3. User can do an action (which we do not need to care what the action is), completing this action will increase the user’s score.
    4. Upon completion the action will dispatch an API call to the application server to update the score.
    5. We want to prevent malicious users from increasing scores without authorisation.
## Implement
    1. Create a backend server with an API that handles requests from clients:
        - The API must include a middleware to verify the valid user.
        - Implement an action in the API that increases the user's score.
        - The API must dispatch an event to check if the new user's score will change the top 10, and update the data if necessary.
    2. Set up a queue system and worker:
        - Process jobs related to updating the top 10 users' scores.
        - Subscribe a message to a socket channel.
    3. Create a socket server:
        - On the socket server, create a channel for receiving and publishing messages related to updating the top 10 users' scores.
## Diagram
    - Please check the diagram in the same folder `problem6.drawio.png`
## Optimizations
    - In the process of checking the cache data for the top 10, we should store the last refresh time.
    - Depending on the refresh time, we can ignore the jobs that were created before this time.
    - Configure the worker to handle this job to run only once each time.